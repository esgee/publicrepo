﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using esgeeNepaliDateTool;
using Windows.UI.StartScreen;
using Windows.UI;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Notifications;
using NotificationsExtensions.Tiles; // NotificationsExtensions.Win10
using NotificationsExtensions;
using Windows.ApplicationModel.Background;
using Windows.UI.Xaml.Media.Animation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace meroCalendar
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    
    public sealed partial class MainPage : Page
    {
        DispatcherTimer myTimer;
        public MainPage()
        {
            this.InitializeComponent();
            //if (Window.Current.Bounds.Width < 580)
            //{
            //    //header.Height = new GridLength(40);
            //    if (Window.Current.Bounds.Height < 650)
            //        time.Visibility = Visibility.Collapsed;
            //}
            myTimer = new DispatcherTimer();
            myTimer.Interval = new TimeSpan(1000);
            myTimer.Tick += MyTimer_Tick;
            myTimer.Start();

            

            // myStoryboard.SetValue(secondHand.Angle, DateTime.Now.Second * 6);
            //loadTile(this);
            updateTile(true);
            // periodicUpdateTile();
            registerBgTask();
            
        }
        private void setStoryboard()
        {
            Storyboard sb = new Storyboard();

        }

        private async void registerBgTask()
        {
            BackgroundExecutionManager.RemoveAccess();
            var result= await BackgroundExecutionManager.RequestAccessAsync();

           // TimeTrigger trigger = new TimeTrigger(15, false);
             SystemTrigger trigger = new SystemTrigger(SystemTriggerType.UserPresent, false);
            // SystemTrigger trigger = new SystemTrigger(SystemTriggerType.InternetAvailable, false);
            var regStatus = Utilities.RegisterBackgroundTask("MyBgTask.TileUpdateBackgroundTask", "meroCalendarTileUpdater", trigger, null);
           
        }



        //private void scheduleUpdate()
        //{
        //    var offset = new TimeSpan(0, 1, 0);
        //    var now = DateTime.Now ;
        //    var then = now.Add(offset);
        //    var dueTime = new DateTimeOffset(then);

        //    // Create the notification object.
        //    var futureTile = new ScheduledTileNotification(TileClass.getXml(NepaliDate.AD2BS(then),then), dueTime);
        //    //futureTile.id = "Tile" + idNumber;

        //    // Add to the schedule.
        //    TileUpdateManager.CreateTileUpdaterForApplication().AddToSchedule(futureTile);
        //}
        private void updateTile(bool CustomTile)
        {
           
            if(CustomTile)
            {
                MyBgTask.TileUpdateBackgroundTask.UpdateTile(true);
                return;
            }

        }
        //private void periodicUpdateTile()
        //{
        //    var reqInterval = PeriodicUpdateRecurrence.HalfHour;
        //    var updater = TileUpdateManager.CreateTileUpdaterForApplication();
        //    updater.StartPeriodicUpdate(null, reqInterval);
        //}

        //private async void loadTile(FrameworkElement sender)
        //{
        //    string id = "smart.esgee";
        //    SecondaryTile tile = new SecondaryTile(id, "Suman Gyawali", id, new Uri("ms-appx:///"), TileSize.Default);
        //    tile.VisualElements.BackgroundColor = Colors.Maroon;
        //    tile.VisualElements.ForegroundText = ForegroundText.Light;
        //    tile.VisualElements.ShowNameOnSquare150x150Logo = true;
        //    tile.VisualElements.ShowNameOnSquare310x310Logo = true;
        //    tile.VisualElements.ShowNameOnWide310x150Logo = true;
          
        //    await tile.RequestCreateForSelectionAsync(getElementRect(this));
        //    //display.Items.Add(new Item { Id = tile.TileId, Content = value, Colour = new SolidColorBrush(background) });
        ////}
        //public static Rect getElementRect(FrameworkElement element)
        //{
        //    GeneralTransform buttonTransform = element.TransformToVisual(null);
        //    Point point = buttonTransform.TransformPoint(new Point());
        //    return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        //}
        private void MyTimer_Tick(object sender, object e)
        {
            DateTime dt = DateTime.Now;
            NepaliDate nDate = NepaliDate.AD2BS(dt);
            MeroDateTime myDate = new MeroDateTime()
            {
                year = dt.Year,
                month = dt.ToString("MMM") + ", " + dt.Year,
                date = dt.Day,
                day = dt.DayOfWeek.ToString(),
                time = dt.ToString("hh:mm"),
                sec = dt.ToString("ss"),
                ampm = dt.ToString("tt"),
                Baar = NepaliDate.getNepaliBaar(dt.DayOfWeek),
                Gate=nDate.ToString("d"),
                Mahina=nDate.ToString("mmm, yyyy")
            };
            grdMain.DataContext = myDate;
            secondHand.Angle = DateTime.Now.Second * 6;
            minuteHand.Angle = (dt.Minute * 6 + secondHand.Angle/60.0)%360;
            hourHand.Angle =  ((dt.Hour + dt.Minute/60.0) * 30)%360;

        }
       

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            myTimer.Stop();
        }

        private void txtHeader_Tapped(object sender, TappedRoutedEventArgs e)
        {
            brdrAbout.Width = Window.Current.Bounds.Width;
            brdrAbout.Height = Window.Current.Bounds.Height;
            about.IsOpen = true;
        }

        private void popup_close(object sender, TappedRoutedEventArgs e)
        {
            about.IsOpen = false;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //DateTime now = DateTime.Now;
            //TimeSpan ts = DateTime.Now.TimeOfDay;

            //storyBoardSecond.Begin();
            //storyBoardMinute.Begin();
            //storyBoardHour.Begin();

            //int ss = DateTime.Now.Second;
            //storyBoardSecond.Seek(new TimeSpan(0, 0, ss));

            //int mm = DateTime.Now.Minute;
            //storyBoardMinute.Seek(new TimeSpan(0, mm, ss));

            //int hh = DateTime.Now.Hour;
            //storyBoardHour.Seek(new TimeSpan(hh, mm, ss));

        }
    }

    class MeroDateTime
    {
        public int year { get; set; }
        public string month { get; set; }
        public int date { get; set; }
        public string day { get; set; }
        public string time { get; set; }
        public string sec { get; set; }
        public string ampm { get; set; }
        public string Baar { get; set; }
        public string Gate { get; set; }
        public string Mahina { get; set; }

    }
}
