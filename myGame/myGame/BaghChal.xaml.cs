﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace meroGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BaghChal : Page
    {
        double sf; //scale factor
        int scale = 60;
        int goatKilled = 0, goatOnHand = 20;
        int winner = 0;
        int currPlayer = 1;
        Border current,prev;
        Border[] goats;// = { goat1, goat2, goat3, goat4, goat5 };
        bool item_tapped = false;
        int[,] board = new int[,]
        {
            { 1,8,8,8,1},
            { 8,8,8,8,8},
            { 8,8,8,8,8},
            { 8,8,8,8,8},
            { 1,8,8,8,1},
        };
        Border[,] Board;
        
        int row, col; // current board index
        //int row1, col1; // prev board index
        Point p1;
        public BaghChal()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            double ht = Window.Current.Bounds.Height;
            double wd = Window.Current.Bounds.Width;
            sf = (Math.Min(ht, wd) / 5)/60.0;
            scale = (int)(sf * 60);
            gridMain.Width = gridMain.Height = sf * gridMain.Width;
            goats = new Border[]{ goat1, goat2, goat3, goat4, goat5, goat6, goat7, goat8, goat9, goat10,
                                 goat11, goat12, goat13, goat14, goat15, goat16, goat17, goat18, goat19, goat20};

            

           // txtRemaining.Text = goatOnHand.ToString() ;
           // txtKilled.Text = goatKilled.ToString();
            InitializeUI();
           
        }
        private void InitializeUI()
        {
            
            #region lines
            line1.X1 = line1.Y1 = line1.X2 *= sf ; line1.Y2 *= sf;
            line2.X1 = line2.X2 *= sf; line2.Y1 *= sf; line2.Y2 *= sf;
            line3.X1 = line3.X2 *= sf; line3.Y1 *= sf; line3.Y2 *= sf;
            line4.X1 = line4.X2 *= sf; line4.Y1 *= sf; line4.Y2 *= sf;
            line5.X1 = line5.X2 = line5.Y2 *= sf; line5.Y1 *= sf;

            line6.Y1 = line6.X1 = line6.Y2 *= sf; line6.X2 *= sf ;
            line7.Y1 = line7.Y2 *= sf ; line7.X1 *= sf ; line7.X2 *= sf;
            line8.Y1 = line8.Y2 *= sf; line8.X1 *= sf ; line8.X2 *= sf;
            line9.Y1 = line9.Y2 *= sf; line9.X1 *= sf; line9.X2 *= sf ;
            line10.Y1 = line10.Y2 = line10.X2 *= sf; line10.X1 *= sf ;

            lineX1.X1 = lineX1.Y1 *= sf; lineX1.X2 = lineX1.Y2 *= sf;
            lineX2.X1 = lineX2.Y2 *= sf; lineX2.X2 = lineX2.Y1 *= sf;

            lineD1.X1 = lineD1.Y2 *= sf; lineD1.X2 = lineD1.Y1 *= sf ;
            lineD3.X1 = lineD3.Y2 *= sf; lineD3.X2 = lineD3.Y1 *= sf ;
            lineD2.X1 = lineD2.Y2 *= sf; lineD2.X2 *= sf ; lineD2.Y1 *= sf;
            lineD4.Y1 = lineD4.X2 *= sf; lineD4.Y2 *= sf ; lineD4.X1 *= sf;
            #endregion


            tiger1.Width = tiger1.Height = tiger2.Width = tiger2.Height= tiger1.Width * sf;
            tiger3.Width = tiger3.Height = tiger4.Width = tiger4.Height = tiger3.Width * sf;

            foreach (Border g in goats)
            {
                g.Width = g.Height = g.Width * sf;
                g.CornerRadius = new CornerRadius(g.Width / 2);
            }
            Board = new Border[5, 5];
            Board[0, 0] = tiger1;
            Board[0, 4] = tiger2;
            Board[4, 0] = tiger3;
            Board[4, 4] = tiger4;

            tiger1.Margin = new Thickness(tiger1.Margin.Left * sf, tiger1.Margin.Top * sf, 0, 0);
            tiger2.Margin = new Thickness(tiger2.Margin.Left * sf, tiger2.Margin.Top * sf, 0, 0);
            tiger3.Margin = new Thickness(tiger3.Margin.Left * sf, tiger3.Margin.Top * sf, 0, 0);
            tiger4.Margin = new Thickness(tiger4.Margin.Left * sf, tiger4.Margin.Top * sf, 0, 0);

            tiger1.CornerRadius = tiger2.CornerRadius = tiger3.CornerRadius = tiger4.CornerRadius = new CornerRadius(tiger1.Width / 2);

            // goat1.Width = goat1.Height = goat1.Width * scale / 60;
            // gridMain.Children.Add(goats[0]);
        }
        
        private void updateBoard(int r,int c)
        {
            Board[row, col] = null;
            Board[r, c] = current;
            //board[row, col] = 8;
            //board[r, c] = currPlayer - 1;
        }
        private bool isEmpty(int r, int c)
        {
            if (Board[r, c] == null)
                return true;
            else
                return false;
        }

        private void placeGoat(int ro,int co)
        {
            if (!isEmpty(ro, co))
                return;
            int r = ro - 2;
            int c = co - 2;
            foreach (Border goat in goats)
            {
                if(goat.Margin.Top>=1000)
                {
                    goat.Margin = new Thickness(c*115*sf,r*115*sf,0,0);
                    Board[ro, co] = goat;
                    goatOnHand--;
                    break;
                }
            }
            if(panelStart.Visibility==Visibility.Visible)
               panelStart.Visibility = Visibility.Collapsed;
        }
       
        private void move(double dx, double dy)
        {
            current.Margin=new Thickness(current.Margin.Left+dx*115*sf,current.Margin.Top+dy*115*sf,0,0);
        }
        private void gridMain_Tapped(object sender, TappedRoutedEventArgs e)
        {

            if (!item_tapped)
            {

                Point p = e.GetPosition(gridMain);
                int r, c;
                c = (int)(p.X / scale);
                r = (int)(p.Y / scale);
                p = new Point((scale / 2) + (c * scale), (scale / 2) + (r * scale));

                    if (goatOnHand > 0 && currPlayer==1) //place goat
                    {
                        if(isEmpty(r,c))
                        {
                            placeGoat(r,c);
                            txtRemaining.Text = goatOnHand.ToString();
                            changePlayerTurn();
                            checkResult();
                        }
                            return;
                    }


                if (p1.X == 0) //already tapped on grid, should select an item first to proceed
                    return;

                double dx = p.X - p1.X;
                double dy = p.Y - p1.Y;
                bool longJump = false;
                if(Math.Abs(dx)>=115*sf || Math.Abs(dy)>=115*sf)
                {
                    longJump = true;
                }

                #region setDataForMove (dx & dy)

                if (Math.Abs(Math.Abs(dx) - Math.Abs(dy)) < (scale / 5)) //move diagonally
                {
                    if ((row + col) % 2 == 0) //able to move diagonally
                    {
                        if (dx > 0 && dy > 0) //down right
                        {
                            dx = 1; dy = 1;
                        }
                        else if (dx < 0 && dy > 0) //down left
                        {
                            dx = -1; dy = 1;
                        }
                        else if (dx > 0 && dy < 0) //up right
                        {
                            dx = 1; dy = -1;

                        }
                        else //(dx < 0 && dy < 0) //up left
                        {
                            dx = -1; dy = -1;
                        }
                    }
                    else
                    {
                        dx = dy = 0;
                    }
                }
                else
                {

                    if ((Math.Abs(dx)) > (Math.Abs(dy)))
                    {
                        dy = 0;
                        if (dx > 0)
                            dx = 1;
                        else if (dx < 0)
                            dx = -1;
                    }
                    else
                    {
                        dx = 0;
                        if (dy > 0)
                            dy = 1;
                        else if (dy < 0)
                            dy = -1;
                    }
                } 
                #endregion

                if(longJump && currPlayer==2)
                {
                    //check if goat present
                    if (Board[row + (int)dy, col + (int)dx] != null)
                    {
                        if (Board[row + (int)dy, col + (int)dx].Name.Contains("goat"))
                        {
                            //check if next position is empty
                            if (Board[row + (int)dy * 2, col + (int)dx * 2] == null)
                            {
                                killGoat(row + (int)dy, col + (int)dx);
                                move(dx * 2, dy * 2);
                                updateBoard(row + (int)dy*2, col + (int)dx*2);
                                current.CornerRadius = new CornerRadius(current.Width/2);
                                changePlayerTurn();
                                checkResult();
                                return;
                            }
                        }
                    }
                }

               
                if(isEmpty(row+(int)dy,col+(int)dx))
                {
                    move(dx, dy);
                    updateBoard(row + (int)dy, col + (int)dx);
                    current.CornerRadius = new CornerRadius(current.Width/2);
                if(dx!=0 || dy!=0)
                    changePlayerTurn();

                    checkResult();
                }
               // p1.X = 0;
            }
            item_tapped = false;

        }
        private void killGoat(int r,int c)
        {
            Board[r, c].Visibility = Visibility.Collapsed;
            Board[r, c] = null;
           
            goatKilled++ ;
            txtKilled.Text = goatKilled.ToString();
        }
        private void changePlayerTurn()
        {
            currPlayer = currPlayer == 1 ? 2 : 1;
            bool isGoat= currPlayer == 1 ? true : false;
            turnOf1.Visibility = isGoat ? Visibility.Visible : Visibility.Collapsed;
            turnOf2.Visibility = isGoat ? Visibility.Collapsed: Visibility.Visible ;
            p1.X = 0;
            prev = null;

        }
        private void checkResult()
        {
            if (goatKilled >= 6)
                winner = 2;
            if(winner==0)
                if (isTigerBlocked())
                    winner = 1;

            if (winner > 0)
            {
                txt1.Text = "Winner";
                turnOf1.Visibility = winner == 1 ? Visibility.Visible : Visibility.Collapsed;
                turnOf2.Visibility = winner == 2 ? Visibility.Visible : Visibility.Collapsed;
                
                string winnerName = winner == 1 ? "Goat" : "Tiger";
                Popup p = new Popup();
                MessageBox msgBox = new MessageBox(winnerName+" won the game.","Game over!");
                p.Child = msgBox;
                if (!p.IsOpen)
                    p.IsOpen = true;
            }
        }
        private bool isTigerBlocked()
        {
            bool gameOver=false;
            if (goatOnHand+goatKilled < 6)
            {
                gameOver = true;
                for(int i=0;i<5;i++)
                    for(int j=0;j<5;j++)
                    {
                        if(Board[i,j] != null)
                            if ( Board[i, j].Name.Contains("tiger"))
                            {
                                #region checkStraight
                                try
                                {
                                    if (Board[i, j + 1] == null) gameOver= false;
                                    if (Board[i, j + 2] == null) gameOver= false;

                                }
                                catch { }

                                try
                                {
                                    if (Board[i, j - 1] == null) gameOver= false;
                                    if (Board[i, j -2] == null) gameOver= false;
                                }
                                catch { }

                                try
                                {
                                    if (Board[i+1, j] == null) gameOver= false;
                                    if (Board[i+2, j] == null) gameOver= false;
                                }
                                catch { }
                                try
                                {
                                    if (Board[i-1, j] == null) gameOver= false;
                                    if (Board[i-2, j] == null) gameOver= false;
                                }
                                catch { }

                                #endregion
                            
                                #region Check diagonal
                                if ((i + j) % 2 == 0) // check diagonal
                                {
                                    try
                                    {
                                        if (Board[i + 1, j + 1] == null) gameOver = false;
                                        if (Board[i + 2, j + 2] == null) gameOver = false;
                                    }
                                    catch { }
                                    try
                                    {
                                        if (Board[i + 1, j - 1] == null) gameOver = false;
                                        if (Board[i + 2, j - 2] == null) gameOver = false;
                                    }
                                    catch { }
                                    try
                                    {
                                        if (Board[i - 1, j + 1] == null) gameOver = false;
                                        if (Board[i - 2, j + 2] == null) gameOver = false;
                                    }
                                    catch { }
                                    try
                                    {
                                        if (Board[i - 1, j - 1] == null) gameOver = false;
                                        if (Board[i - 2, j - 2] == null) gameOver = false;
                                    }
                                    catch { }
                                
                                }
                                #endregion
                               // gameOver = true;
                            }
                    }
            }
            //last:
            return gameOver;
        }
        Popup popUp;
        private void jump_start(object sender, TappedRoutedEventArgs e)
        {
                placeGoat(0, 1);
                placeGoat(0, 2);
                placeGoat(1, 4);
                placeGoat(2, 4);
                placeGoat(4, 3);
                placeGoat(4, 2);
                placeGoat(3, 0);
                placeGoat(2, 0);

                txtRemaining.Text = goatOnHand.ToString();

        }
 
        private void PopUp_Closed(object sender, object e)
        {
            MessageBox mb = popUp.Child as MessageBox;
            if (mb.OkTapped == true)
            {
                Frame.Navigate(typeof(BaghChal));
            }
        }

        private void challenge_start(object sender, TappedRoutedEventArgs e)
        {
                placeGoat(2, 2);
                txtRemaining.Text = goatOnHand.ToString();
                changePlayerTurn();
        }

        private void restart_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (goatOnHand < 20 && winner==0)
            {
                if (popUp == null) popUp = new Popup();
                MessageBox msgbox = new MessageBox("Current progress will be lost.\nProceed anyway?", "Confirm!", true);
                popUp.Child = msgbox;
                if (!popUp.IsOpen)
                    popUp.IsOpen = true;
                popUp.Closed += PopUp_Closed;
            }
            else
                Frame.Navigate(typeof(BaghChal));
                
        }

        private void goat_tapped(object sender, TappedRoutedEventArgs e)
        {
            item_tapped = true;
            if (goatOnHand > 0 || currPlayer==2)
                return;
            
            current = sender as Border;
            if (prev != null)
                prev.CornerRadius = new CornerRadius(prev.Width/2);
            prev = current;
            current.CornerRadius = new CornerRadius(current.Width/4);

            p1 = e.GetPosition(gridMain);
            col = (int)(p1.X / scale);
            row = (int)(p1.Y / scale);
            p1 = new Point((scale / 2) + (col * scale), (scale / 2) + (row * scale));
        }

        private void tiger_tapped(object sender, TappedRoutedEventArgs e)
        {
            item_tapped = true;
            if (currPlayer == 1)  
                return;


            current = sender as Border;
            if (prev != null)
                prev.CornerRadius = new CornerRadius(prev.Width/2);
            prev = current;
            current.CornerRadius = new CornerRadius(current.Width/4);

            p1 = e.GetPosition(gridMain);
            col = (int)(p1.X / scale);
            row = (int)(p1.Y / scale);
            p1 = new Point((scale / 2) + (col * scale), (scale / 2) + (row * scale));


        }

        private void back_Tapped(object sender, TappedRoutedEventArgs e)
        {
            // release resource
            Frame.Navigate(typeof(MainPage));
        }
    }
}
